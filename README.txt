-- SUMMARY --

The Commerce WeDeal module allows iDeal payments
via WeDeal PSP (dutch) on Commerce.
Easy implementation of the iDeal service from WeDeal.

-- REQUIREMENTS --

commerce
  -- commerce_ui
  -- commerce_payment
  -- commerce_order

-- INSTALLATION --

Standard Drupal module installation, see http://drupal.org/node/70151.
Downloading of the Commerce module
Installing of the Commerce Module

-- CONFIGURATION --

* Enable the commerce_wedeal module
* Visit admin/commerce/config/wedeal for WeDeal configuration
* Visit admin/commerce/config/payment-methods
* Click add Payment Method rule
* Add a Payment Rule
* On "Actions", click "Add Action"
* In the dropdown, select "Enable payment method: WeDeal"
or one of the other WeDeal methods you enabled
* Click save

-- CONTACT --

Current maintainers:
* Jeroen Bobbeldijk (jeroen.b) - http://drupal.org/user/1853532

